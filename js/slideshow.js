/**
 * @file Slideshows.
 * Uses Drupal.behaviors
 * @see customer/README.md
 * @see https://www.drupal.org/docs/8/api/javascript-api/javascript-api-overview
 */

'use strict';

(function (Drupal, drupalSettings) {

  Drupal.behaviors.entitySlideshow = {

    attach: function (context) {
      once('entity-gallery-slideshow', '.entity-gallery-slideshow').forEach(slideshowElement => {
        const settings = {
          spaceBetween: 10,
          loop: true,
          pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
          },
          navigation: {
            prevEl: '.swiper-button-prev',
            nextEl: '.swiper-button-next',
            clickable: false,
          },
          scrollbar: {
            el: '.swiper-scrollbar',
            draggable: true,
          },
          a11y: {
            prevSlideMessage: Drupal.t('Previous slide'),
            nextSlideMessage: Drupal.t('Next slide'),
            slideLabelMessage: '{{index}} / {{slidesLength}}',
            slideRole: 'group',
            itemRoleDescriptionMessage: Drupal.t('slide'),
          }
        }

        const slideshow = new Swiper(slideshowElement, settings);
        initialiszePosition(slideshowElement);
        swiperA11y(slideshow);
      });

      /**
       * Set the first displayed slide according to clicked one.
       *
       * @param slideshowElement
       *   The slideshow element.
       */
      function initialiszePosition(slideshowElement) {
        const slideshow = slideshowElement.swiper;
        slideshow.slideTo(slideshowElement.dataset.slide)
      }

      /**
       * Applies A11Y behaviours on slides.
       *
       * @param slideshow
       */
      function swiperA11y(slideshow) {
        const slides = slideshow.slides;
        const activeSlide = slides[slideshow.activeIndex];

        // Initializing accessibility for the active slide.
        handleFocusableElements(activeSlide, true);

        // Disable accessibility for other slides.
        for (let i = 0; i < slides.length; i++) {
          const slide = slides[i];
          if (slide !== activeSlide) {
            handleFocusableElements(slide, false);
          }
        }

        // Manage focusable elements when changing slides.
        slideshow.on('slideChange', function () {
          const newPreviousSlide = slideshow.slides[slideshow.previousIndex];
          const newActiveSlide = slideshow.slides[slideshow.activeIndex];
          // Disable accessibility on previous slide
          handleFocusableElements(newPreviousSlide, false);
          // Enable accessibility on the new active slide.
          handleFocusableElements(newActiveSlide, true);
        });
      }

      /**
       * Manage focusable elements in a slide.
       *
       * @param slide
       *   The slide to manage.
       * @param isActive
       *   Whether the slide is active.
       */
      function handleFocusableElements(slide, isActive) {
        const focusableElements = slide.querySelectorAll('a, button');
        focusableElements.forEach(element => {
          if (isActive) {
            element.removeAttribute('tabindex');
          } else {
            element.setAttribute('tabindex', '-1');
          }
        });
      }
    }
  };

})(Drupal, drupalSettings);
