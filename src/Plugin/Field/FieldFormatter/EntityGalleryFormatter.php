<?php

namespace Drupal\entity_gallery_slideshow\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'Gallery' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_gallery_slideshow_view",
 *   label = @Translation("Gallery"),
 *   description = @Translation("Display the referenced entities as a gallery &
 *   slideshow."), field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityGalleryFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'gallery_view_mode' => 'default',
      'slideshow_view_mode' => 'default',
      'modal_title' => t('Slideshow'),
      'pager' => TRUE,
      'progress_bar' => FALSE,
    ] + EntityReferenceFormatterBase::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements['gallery_view_mode'] = [
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
      '#title' => $this->t('Gallery view mode'),
      '#default_value' => $this->getSetting('gallery_view_mode'),
      '#required' => TRUE,
    ];

    $elements['slideshow_view_mode'] = [
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
      '#title' => $this->t('Slideshow view mode'),
      '#default_value' => $this->getSetting('slideshow_view_mode'),
      '#required' => TRUE,
    ];

    $elements['modal_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Modal title'),
      '#default_value' => $this->getSetting('modal_title'),
      '#required' => TRUE,
    ];

    $elements['pager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable pager'),
      '#default_value' => $this->getSetting('pager'),
    ];

    $elements['progress_bar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable progress bar'),
      '#default_value' => $this->getSetting('progress_bar'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $view_modes = $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type'));

    $gallery_view_mode = $this->getSetting('gallery_view_mode');
    $summary[] = $this->t('Mozaic view mode: @mode', [
      '@mode' => $view_modes[$gallery_view_mode] ?? $gallery_view_mode,
    ]);

    $slideshow_view_mode = $this->getSetting('slideshow_view_mode');
    $summary[] = $this->t('Gallery view mode: @mode', [
      '@mode' => $view_modes[$slideshow_view_mode] ?? $slideshow_view_mode,
    ]);

    $modal_title = $this->getSetting('modal_title');
    if (!empty($modal_title)) {
      $summary[] = $this->t('Modal title: @title', [
        '@title' => $modal_title,
      ]);
    }

    $summary[] = $this->t('Pager: @status', [
      '@status' => $this->getSetting('pager') ? $this->t('Yes') : $this->t('No'),
    ]);

    $summary[] = $this->t('Progress bar: @status', [
      '@status' => $this->getSetting('progress_bar') ? $this->t('Yes') : $this->t('No'),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $view_mode = $this->getSetting('gallery_view_mode');
    $entities = $this->getEntitiesToView($items, $langcode);

    // Remove slashes from modal title, because it can't be passed in URL.
    $modal_title = $this->getSetting('modal_title');
    $modal_title = Xss::filter($modal_title);
    $modal_title = str_replace('/', '_', $modal_title);
    $modal_title = urlencode($modal_title);

    // Wrap results in a ul/li.
    $render = [
      0 => [
        '#theme' => 'item_list',
        '#attributes' => [
          'class' => ['entity-gallery'],
        ],
        '#items' => [],
        '#attached' => ['library' => ['entity_gallery_slideshow/entity_gallery_slideshow']],
      ],
    ];

    foreach ($entities as $delta => $entity) {
      /** @var \Drupal\Core\Entity\EntityInterface $parent_entity */
      $parent_entity = $entity->_referringItem->getParent()
        ->getParent()
        ->getEntity();

      // Wrap each element in a link that will open the slideshow dialog.
      $params = [
        'entity_type' => $parent_entity->getEntityTypeId(),
        'entity_id' => (int) $parent_entity->id(),
        'field_name' => $this->fieldDefinition->getName(),
        'slide_id' => (int) $delta,
        'view_mode' => $this->getSetting('slideshow_view_mode'),
        'pager' => (int) $this->getSetting('pager'),
        'progress_bar' => (int) $this->getSetting('progress_bar'),
        'modal_title' => $modal_title,
      ];
      $options = [
        'attributes' => [
          'class' => ['dialog', 'use-ajax'],
          'data-dialog-type' => 'dialog',
          'data-slide' => $delta,
        ],
      ];
      $url = new Url('entity_gallery_slideshow.slideshow.nojs', $params, $options);

      $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
      $rendered_entity = $view_builder->view($entity, $view_mode, $entity->language()->getId());
      $link = new Link($rendered_entity, $url);

      $render[0]['#items'][] = [
        '#type' => 'container',
        '#wrapper_attributes' => [
          'class' => ['entity-gallery-item'],
        ],
        '#attributes' => [
          'class' => ['entity-gallery-content'],
        ],
        'content' => $link->toRenderable(),
        '#cache' => [
          'tags' => $entity->getCacheTags(),
        ],
      ];
    }

    // Add AJAX library.
    $render['#attached']['library'] = ['core/drupal.dialog.ajax'];

    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    // Check that entity has view modes.
    $target_type = $field_definition->getFieldStorageDefinition()
      ->getSetting('target_type');
    $view_modes = \Drupal::service('entity_display.repository')
      ->getViewModes($target_type);

    return parent::isApplicable($field_definition) && !empty($view_modes);
  }

}
