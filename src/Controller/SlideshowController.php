<?php

namespace Drupal\entity_gallery_slideshow\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for the slideshow.
 *
 * @package Drupal\entity_gallery_slideshow\Controller
 */
class SlideshowController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SlideshowController {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Return the entities slideshow.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity ID.
   * @param string $field_name
   *   The entity field containing the entities.
   * @param int $slide_id
   *   The clicked slide ID.
   * @param string $view_mode
   *   The view mode to render the entity with.
   * @param int $pager
   *   Whether the pager is enabled.
   * @param int $progress_bar
   *   Whether the progress bar is enabled.
   * @param string $modal_title
   *   The modal title.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns the ajax response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSlideshowAjax(
    string $entity_type,
    int $entity_id,
    string $field_name,
    int $slide_id,
    string $view_mode,
    int $pager,
    int $progress_bar,
    string $modal_title,
  ): AjaxResponse {
    $parent_entity = $this->entityTypeManager->getStorage($entity_type)
      ->load($entity_id);

    if (!$parent_entity instanceof EntityInterface || !$parent_entity->hasField($field_name)) {
      throw new NotFoundHttpException();
    }

    $build = [];

    // Add wrapper to be able to target it with JS.
    $build['wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['entity-gallery-slideshow'],
        'data-slide' => $slide_id,
      ],
      '#attached' => [
        'library' => [
          'entity_gallery_slideshow/slideshow',
        ],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    // Add rendered entities.
    $items = [];
    $entities = $parent_entity->{$field_name}->referencedEntities();

    foreach ($entities as $key => $entity) {
      $render = $this->entityTypeManager()
        ->getViewBuilder($entity->getEntityTypeId())
        ->view($entity, $view_mode);
      $render['#slide_id'] = $key;
      $items[] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['swiper-slide'],
        ],
        'content' => $render,
      ];
    }

    $build['wrapper']['items-wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['swiper-wrapper'],
      ],
      'items' => $items,
    ];

    // Add UI elements.
    if (!empty($pager)) {
      $build['wrapper']['pagination'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['swiper-pagination'],
        ],
      ];
    }

    $build['wrapper']['button-prev'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['swiper-button-prev'],
      ],
    ];

    $build['wrapper']['button-next'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['swiper-button-next'],
      ],
    ];

    if (!empty($progress_bar)) {
      $build['wrapper']['progress-bar'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['swiper-scrollbar'],
        ],
      ];
    }

    // Send response.
    $response = new AjaxResponse();
    $dialog_options = [
      'title' => urldecode($modal_title),
      'closeText' => $this->t('Close slideshow'),
      'modal' => TRUE,
      'dialogClass' => 'gallery-slideshow-dialog',
      'width' => '90vw',
    ];
    $response->addCommand(new OpenModalDialogCommand($this->t('Slideshow'), $build, $dialog_options));

    return $response;
  }

  /**
   * Return the requested slide.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity ID.
   * @param string $field_name
   *   The entity field containing the entities.
   * @param int $slide_id
   *   The clicked slide ID.
   * @param string $view_mode
   *   The view mode to render the entity with.
   * @param int $pager
   *   Whether the pager is enabled.
   * @param int $progress_bar
   *   Whether the progress bar is enabled.
   * @param string $modal_title
   *   The modal title.
   *
   * @return array
   *   Returns the slide render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSlideshowNojs(
    string $entity_type,
    int $entity_id,
    string $field_name,
    int $slide_id,
    string $view_mode,
    int $pager,
    int $progress_bar,
    string $modal_title,
  ): array {
    $parent_entity = $this->entityTypeManager->getStorage($entity_type)
      ->load($entity_id);

    if (!$parent_entity instanceof EntityInterface || !$parent_entity->hasField($field_name) ||
      !$parent_entity->{$field_name}->get($slide_id)?->entity instanceof EntityInterface) {
      throw new NotFoundHttpException();
    }

    // Display requested entity.
    $requested_entity = $parent_entity->{$field_name}->get($slide_id)->entity;
    $rendered_entity = $this->entityTypeManager()
      ->getViewBuilder($requested_entity->getEntityTypeId())
      ->view($requested_entity, $view_mode);

    $build = [
      '#type' => 'container',
      '#cache' => [
        'tags' => ["{$entity_type}_list"],
      ],
      'main' => $rendered_entity,
    ];

    // Add navigation links.
    $navigation_links = [];
    $route_params = [
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'field_name' => $field_name,
      'slide_id' => $slide_id,
      'view_mode' => $view_mode,
      'pager' => $pager,
      'progress_bar' => $progress_bar,
      'modal_title' => $modal_title,
    ];

    $previous_slide_id = $slide_id - 1;
    if ($previous_slide_id >= 0) {
      $route_params['slide_id'] = $previous_slide_id;
      $previous_url = new Url('entity_gallery_slideshow.slideshow.nojs', $route_params);
      $navigation_links[] = (new Link($this->t('Previous'), $previous_url))->toRenderable();
    }

    $next_slide_id = $slide_id + 1;
    $items_count = $parent_entity->{$field_name}->count();
    if ($next_slide_id < $items_count) {
      $route_params['slide_id'] = $next_slide_id;
      $next_url = new Url('entity_gallery_slideshow.slideshow.nojs', $route_params);
      $navigation_links[] = (new Link($this->t('Next'), $next_url))->toRenderable();
    }

    if (!empty($navigation_links)) {
      $build['navigation'] = [
        '#theme' => 'item_list',
        '#items' => $navigation_links,
      ];
    }

    return $build;
  }

  /**
   * Return the "nojs" page title.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity ID.
   * @param string $field_name
   *   The entity field containing the entities.
   * @param int $slide_id
   *   The clicked slide ID.
   * @param string $view_mode
   *   The view mode to render the entity with.
   * @param int $pager
   *   Whether the pager is enabled.
   * @param int $progress_bar
   *   Whether the progress bar is enabled.
   * @param string $modal_title
   *   The modal title.
   *
   * @return string
   *   The entity title & pagination.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getSlideshowNojsTitle(
    string $entity_type,
    int $entity_id,
    string $field_name,
    int $slide_id,
    string $view_mode,
    int $pager,
    int $progress_bar,
    string $modal_title,
  ): string {
    $parent_entity = $this->entityTypeManager->getStorage($entity_type)
      ->load($entity_id);
    $requested_entity = $parent_entity->{$field_name}->get($slide_id)->entity;
    $entity_label = $requested_entity->label();
    $index = $slide_id + 1;
    $items_count = $parent_entity->{$field_name}->count();

    return $this->t("@entity_label | Page @index of @count", [
      '@entity_label' => $entity_label,
      '@index' => $index,
      '@count' => $items_count,
    ]);
  }

}
