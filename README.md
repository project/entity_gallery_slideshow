# Entity Gallery to Slideshow

## Contents of this file

 - Introduction
 - Styles
 - Installation
 - Dependencies
 - Maintainers

## Introduction

This module provides a new formatter for your entity reference fields. It allows
you to display "view mode-able" entities as a gallery, and to display these
entities in a modal containing a slideshow using another view mode (for example,
entity details). The slideshow is synced with the clicked element. You can mix
entity bundles, like images and documents (being medias in this example).

## Styles

This module contains only minimal styles. It is your responsability, as the
slideshow is in a Drupal modal (Drupal dependant) and items are view-mode
related (out of scope).

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Dependencies

The module uses now the [Swiper](https://swiperjs.com/) library to replace
Accessible-Slick (that is no more compliant with jQuery 4, shipped with Drupal
11).

## Maintainers

Current maintainers:
- Nicolas N - [nicocin](https://www.drupal.org/u/nicocin)
- myrNG - [myrNG](https://www.drupal.org/u/myrng)
